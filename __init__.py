import gc
import json
import math

import leds
import network
import urequests
from ctx import Context
from st3m.application import Application, ApplicationContext
from st3m.ui.view import ViewManager
from st3m.goose import Any, Dict, Enum, Optional, Tuple
from st3m.input import InputState
from st3m.ui.colours import BLACK, GO_GREEN, PUSH_RED

AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"


class TrainType(Enum):
    ICE = 0
    RAILJET = 1


# Set this to a train type to run without actually attempting WiFi connection.
# FAKE = TrainType.ICE
FAKE = None


class AppState(Enum):
    DETECTING = 0
    CONNECTING = 1
    GET_CSRF = 2
    LOGIN = 3
    RUN = 4
    RUN1 = 5


class App(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.nic = network.WLAN(network.STA_IF)
        self.nic.active(True)
        if FAKE is None:
            self.state: AppState = AppState.DETECTING
            self.train_type: Optional[TrainType] = None
        else:
            self.state = AppState.RUN1
            self.train_type = FAKE
        self.time = 0
        self.last_request_time = 0
        self.bundle_path = app_ctx.bundle_path
        self.train_status: Dict[str, Any] = {}

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        if FAKE is None:
            self.state = AppState.DETECTING
            self.train_type = None
        else:
            self.state = AppState.RUN1
            self.train_type = FAKE

    def draw(self, ctx: Context) -> None:
        if self.train_type == TrainType.ICE:
            ctx.rgb(0.137, 0.161, 0.204)
        elif self.train_type == TrainType.RAILJET:
            ctx.rgb(0.275, 0.275, 0.267)
        else:
            ctx.rgb(0, 0, 0)
        ctx.rectangle(-120, -120, 240, 240).fill()

        if self.state in (
            AppState.DETECTING,
            AppState.CONNECTING,
            AppState.GET_CSRF,
            AppState.LOGIN,
        ):
            ctx.save()
            ctx.rgb(1, 1, 1)
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 30
            ctx.font = ctx.get_font_name(1)
            ctx.move_to(0, 0)
            if self.state == AppState.DETECTING:
                ctx.text("Detecting WiFi...")
            elif self.state == AppState.CONNECTING:
                ctx.text("Connecting...")
            elif self.state == AppState.GET_CSRF:
                ctx.text("Fetching token...")
            elif self.state == AppState.LOGIN:
                ctx.text("Logging in...")
            ctx.restore()
        elif self.state == AppState.RUN:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 1
            ctx.font = ctx.get_font_name(1)

            if self.train_type == TrainType.ICE:
                ctx.rgb(1, 1, 1)
                text = f"{int(self.train_status['speed'])} km/h"
            elif self.train_type == TrainType.RAILJET:
                ctx.rgb(0.969, 0.984, 0.961)
                text = f"{int(self.train_status['speed'])} km/h"
            else:
                text = "???"
            scale = 1 / ctx.text_width(text) * 220

            ctx.save()
            ctx.scale(scale, scale)
            ctx.move_to(0, 0)
            ctx.text(text)
            ctx.restore()

            if self.train_type == TrainType.ICE:
                ctx.save()
                ctx.font = ctx.get_font_name(0)
                ctx.font_size = 16
                ctx.rgb(0.7, 0.7, 0.7)
                ctx.move_to(0, 50)
                ctx.text("Thank you for travelling")
                ctx.move_to(0, 70)
                ctx.text("with Deutsche Bahn!")
                ctx.restore()

            if self.train_type == TrainType.ICE:
                ctx.image(self.bundle_path + "/ICE-Logo.png", -35, -80, 70, 39)
            elif self.train_type == TrainType.RAILJET:
                ctx.image(self.bundle_path + "/Railjet-Logo.png", -58, -80, 116, 30)

            # LED Speed Gauge
            if self.train_type == TrainType.ICE:
                speed_percent = min(self.train_status["speed"] / 260, 1.0)
            elif self.train_type == TrainType.RAILJET:
                speed_percent = min(self.train_status["speed"] / 260, 1.0)
            else:
                speed_percent = 0.0
            for led in range(33):
                if speed_percent == 0:
                    value = 0.0
                else:
                    value = 0.2 if (led / 32) <= speed_percent else 0
                leds.set_rgb((led + 24) % 40, value, 0, 0)

            # Status Indicator
            leds.set_rgb(20, 0, 0.2 if (self.time // 1000) % 2 == 0 else 0, 0)

            leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.time += delta_ms

        if self.state == AppState.DETECTING:
            for net in self.nic.scan():
                if net[0] == b"WIFIonICE":
                    print("Connecting to WIFIonICE!")
                    self.nic.connect(b"WIFIonICE")
                    self.train_type = TrainType.ICE
                    self.state = AppState.CONNECTING
                    break
                elif net[0] == b"OEBB":
                    print("Connecting to OEBB!")
                    self.nic.connect(b"OEBB")
                    self.train_type = TrainType.RAILJET
                    self.state = AppState.CONNECTING
                    break
        elif FAKE is None and not self.nic.isconnected():
            self.state = AppState.CONNECTING
        elif self.state == AppState.CONNECTING and self.nic.isconnected():
            print("Connected!")
            self.state = AppState.GET_CSRF
        elif self.state == AppState.GET_CSRF:
            if self.train_type == TrainType.ICE:
                r = urequests.get("https://login.wifionice.de/de")
                if r.status_code == 200:
                    self.csrf_token = r.headers["Set-Cookie"][5:][:32]
                    print(f"Got token: {self.csrf_token}")
                    self.state = AppState.LOGIN
                del r
                gc.collect()
            else:
                print("Skipping GET_CSRF for this train type?")
                self.state = AppState.LOGIN
        elif self.state == AppState.LOGIN:
            if self.train_type == TrainType.ICE:
                r = urequests.post(
                    "https://login.wifionice.de/de",
                    data=f"login=true&CSRFToken={self.csrf_token}",
                )
                if r.status_code == 200:
                    print("Logged in!")
                    self.state = AppState.RUN1
                del r
                gc.collect()
            else:
                print("Skipping LOGIN for this train type?")
                self.state = AppState.RUN1
        elif self.state in (AppState.RUN, AppState.RUN1):
            if (self.time - self.last_request_time) > 100 if FAKE else 2500:
                self.last_request_time = self.time
                if FAKE is not None:
                    self.train_status = {
                        "speed": (
                            math.sin(self.time / 1000 * math.pi * 2 / 10) * 0.5 + 0.5
                        )
                        * 340.0,
                    }
                    self.state = AppState.RUN
                elif self.train_type == TrainType.ICE:
                    try:
                        res = urequests.get(
                            "https://iceportal.de/api1/rs/status",
                            headers={"User-Agent": AGENT},
                        )
                        if res.status_code == 200:
                            self.train_status = res.json()
                            print(repr(self.train_status))
                            self.state = AppState.RUN
                        del res
                    except OSError as e:
                        print("Error while fetching iceportal status:", str(e))
                    gc.collect()
                elif self.train_type == TrainType.RAILJET:
                    res = urequests.get(
                        "https://railnet.oebb.at/api/speed",
                        headers={"User-Agent": AGENT},
                    )
                    if res.status_code == 200:
                        self.train_status = {"speed": float(res.text)}
                        print(repr(self.train_status))
                        self.state = AppState.RUN
                    del res
                    gc.collect()
                else:
                    print("Can't fetch data for this train type?")


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(App(ApplicationContext()))
